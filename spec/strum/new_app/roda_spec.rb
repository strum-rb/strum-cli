# frozen_string_literal: true

RSpec.describe "Roda stack" do
  let(:task) { Strum::CLI::New }

  describe "clean" do
    before :each do
      FileUtils.rm_rf(TEST_APP_PATH) if File.directory?(TEST_APP_PATH)
      FileUtils.chdir(TEST_PATH) { task.new([TEST_APP], stack: ["roda"]).invoke_all }
    end

    it "empty project" do
      expect(File.directory?(TEST_APP_PATH)).to be
      expect(File.exist?("#{TEST_APP_PATH}/app/roda.rb")).to be
      expect(File.exist?("#{TEST_APP_PATH}/config/puma.rb")).to be
      Dir.chdir(TEST_APP_PATH) do
        Bundler.with_unbundled_env do
          expect(system("bundle install")).to be(true)
          expect(system("bundle exec rubocop")).to be(true)
        end
      end
    end
  end

  describe "+ SequelPG" do
    before :each do
      FileUtils.rm_rf(TEST_APP_PATH) if File.directory?(TEST_APP_PATH)
      FileUtils.chdir(TEST_PATH) { task.new([TEST_APP], stack: %w[roda sequel_pg]).invoke_all }
    end

    it "empty project" do
      expect(File.directory?(TEST_APP_PATH)).to be
      expect(File.exist?("#{TEST_APP_PATH}/app/roda.rb")).to be
      expect(File.exist?("#{TEST_APP_PATH}/config/puma.rb")).to be
      Dir.chdir(TEST_APP_PATH) do
        Bundler.with_unbundled_env do
          expect(system("bundle install")).to be(true)
          expect(system("bundle exec rubocop")).to be(true)
        end
      end
      expect(File.read("#{TEST_APP_PATH}/config.ru")).to include("Rack::Unreloader.new(subclasses: %w[Roda Sequel::Model]")
    end
  end
end
