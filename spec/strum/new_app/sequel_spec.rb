# frozen_string_literal: true

RSpec.describe "SequelPG stack" do
  let(:task) { Strum::CLI::New }

  describe "clean" do
    before :each do
      FileUtils.rm_rf(TEST_APP_PATH) if File.directory?(TEST_APP_PATH)
      FileUtils.chdir(TEST_PATH) { task.new([TEST_APP], stack: ["sequel_pg"]).invoke_all }
    end

    it "empty project" do
      expect(File.directory?(TEST_APP_PATH)).to be
      expect(File.exist?("#{TEST_APP_PATH}/config/sequel.rb")).to be
      Dir.chdir(TEST_APP_PATH) do
        Bundler.with_unbundled_env do
          expect(system("bundle install")).to be(true)
          expect(system("bundle exec rubocop")).to be(true)
        end
      end
    end
  end

  describe "+ Roda" do
    before :each do
      FileUtils.rm_rf(TEST_APP_PATH) if File.directory?(TEST_APP_PATH)
      FileUtils.chdir(TEST_PATH) { task.new([TEST_APP], stack: %w[sequel_pg roda]).invoke_all }
    end

    it "empty project" do
      expect(File.directory?(TEST_APP_PATH)).to be
      expect(File.exist?("#{TEST_APP_PATH}/config/sequel.rb")).to be
      Dir.chdir(TEST_APP_PATH) do
        Bundler.with_unbundled_env do
          expect(system("bundle install")).to be(true)
          expect(system("bundle exec rubocop")).to be(true)
        end
      end
      expect(File.read("#{TEST_APP_PATH}/config.ru")).to include("Rack::Unreloader.new(subclasses: %w[Roda Sequel::Model]")
    end
  end

end
