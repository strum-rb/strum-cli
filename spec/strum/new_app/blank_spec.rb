# frozen_string_literal: true

RSpec.describe Strum::CLI::New do
  let(:task) { Strum::CLI::New }

  before :each do
    FileUtils.rm_rf(TEST_APP_PATH) if File.directory?(TEST_APP_PATH)
    FileUtils.chdir(TEST_PATH) { task.new([TEST_APP]).invoke_all }
  end

  it "empty project" do
    expect(File.directory?(TEST_APP_PATH)).to be
    expect(File.directory?(File.join(TEST_APP_PATH, "bin"))).to be
    expect(File.directory?(File.join(TEST_APP_PATH, "config"))).to be
    expect(File.directory?(File.join(TEST_APP_PATH, "lib"))).to be
    expect(File.directory?(File.join(TEST_APP_PATH, "services"))).to be
    expect(File.exist?("#{TEST_APP_PATH}/Gemfile")).to be
    expect(File.exist?("#{TEST_APP_PATH}/Rakefile")).to be
    Dir.chdir(TEST_APP_PATH) { Bundler.with_unbundled_env { expect(system("bundle exec rubocop")).to be(true) } }

  end
end
