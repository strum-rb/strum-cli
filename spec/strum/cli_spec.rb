# frozen_string_literal: true

RSpec.describe Strum::CLI do
  it "has a version number" do
    expect(Strum::CLI::VERSION).not_to be nil
  end
end
