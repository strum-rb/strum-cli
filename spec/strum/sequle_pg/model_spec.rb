# frozen_string_literal: true

require "strum/cli"

RSpec.describe Strum::CLI::Generator::Model do
  let(:generator_model) { Strum::CLI::Generator::Model }
  let(:task) { Strum::CLI::New }

  before :each do
    FileUtils.rm_rf(TEST_APP_PATH) if File.directory?(TEST_APP_PATH)
    FileUtils.chdir(TEST_PATH) { task.new([TEST_APP], stack: ["sequel_pg"]).invoke_all }
  end

  it "create model by name FirstTestEntity" do
    Dir.chdir(TEST_APP_PATH) { generator_model.new(["FirstTestEntity"]).invoke_all }
    expect(File.exist?("#{TEST_APP_PATH}/models/first_test_entity.rb")).to be
    expect(File.exist?("#{TEST_APP_PATH}/db/migrations/010_create_first_test_entities.rb")).to be
  end

  it "create model with namespace second_test_entity" do
    Dir.chdir(TEST_APP_PATH) { generator_model.new(["second_test_entity"]).invoke_all }
    expect(File.exist?("#{TEST_APP_PATH}/models/second/test/entity.rb")).to be
    expect(File.exist?("#{TEST_APP_PATH}/db/migrations/010_create_second_test_entities.rb")).to be
  end

  it "create model with namespace Third::Test::MyEntity" do
    Dir.chdir(TEST_APP_PATH) { generator_model.new(["Third::Test::MyEntity"]).invoke_all }
    expect(File.exist?("#{TEST_APP_PATH}/models/third/test/my_entity.rb")).to be
    expect(File.exist?("#{TEST_APP_PATH}/db/migrations/010_create_third_test_my_entities.rb")).to be
  end

  it "create model by name FirstTestEntity without migration" do
    Dir.chdir(TEST_APP_PATH) { generator_model.new(["FirstTestEntity"], migration: false).invoke_all }
    expect(File.exist?("#{TEST_APP_PATH}/models/first_test_entity.rb")).to be
    expect(File.exist?("#{TEST_APP_PATH}/db/migrations/010_create_first_test_entities.rb")).not_to be
  end
end
