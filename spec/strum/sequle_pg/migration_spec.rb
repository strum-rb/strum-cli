# frozen_string_literal: true

require "strum/cli"

RSpec.describe Strum::CLI::Generator::Model do
  let(:generator_migration) { Strum::CLI::Generator::Migration }
  let(:task) { Strum::CLI::New }

  before :each do
    FileUtils.rm_rf(TEST_APP_PATH) if File.directory?(TEST_APP_PATH)
    FileUtils.chdir(TEST_PATH) { task.new([TEST_APP], stack: ["sequel_pg"]).invoke_all }
  end

  it "create clean migration by name FirstTestMigration" do
    Dir.chdir(TEST_APP_PATH) { generator_migration.new(["FirstTestMigration"]).invoke_all }
    expect(File.exist?("#{TEST_APP_PATH}/db/migrations/010_first_test_migration.rb")).to be
  end

  it "create clean migration with underscore namespace second_test_migration" do
    Dir.chdir(TEST_APP_PATH) { generator_migration.new(["second_test_migration"]).invoke_all }
    expect(File.exist?("#{TEST_APP_PATH}/db/migrations/010_second_test_migration.rb")).to be
  end

  it "create clean migration with namespace Third::Test::MyMigration" do
    Dir.chdir(TEST_APP_PATH) { generator_migration.new(["Third::Test::MyMigration"]).invoke_all }
    expect(File.exist?("#{TEST_APP_PATH}/db/migrations/010_third_test_my_migration.rb")).to be
  end

  it "create model migration by name FirstTestEntity" do
    Dir.chdir(TEST_APP_PATH) { generator_migration.new(["FirstTestEntity"], model: "FirstTestEntity").invoke_all }
    expect(File.exist?("#{TEST_APP_PATH}/db/migrations/010_create_first_test_entities.rb")).to be
  end

  it "create model migration with namespace First::TestEntity" do
    Dir.chdir(TEST_APP_PATH) { generator_migration.new(["First::TestEntity"], model: "First::TestEntity").invoke_all }
    expect(File.exist?("#{TEST_APP_PATH}/db/migrations/010_create_first_test_entities.rb")).to be
  end

end
