# frozen_string_literal: true

require "bundler/setup"
require "strum/cli"

TEST_PATH = "tmp".freeze
TEST_APP = "dummy".freeze
TEST_APP_PATH = File.join(TEST_PATH, TEST_APP).freeze

RSpec.configure do |config|
  # Enable flags like --only-failures and --next-failure
  config.example_status_persistence_file_path = ".rspec_status"

  # Disable RSpec exposing methods globally on `Module` and `main`
  config.disable_monkey_patching!

  config.expect_with :rspec do |c|
    c.syntax = :expect
  end

  config.before(:suite) do
    FileUtils.mkdir_p(TEST_PATH)
  end

  config.after(:suite) do
    FileUtils.rm_rf(TEST_APP_PATH) if File.directory?(TEST_APP_PATH)
  end
end
