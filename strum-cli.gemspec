# frozen_string_literal: true

require_relative "lib/strum/cli/version"

Gem::Specification.new do |spec|
  spec.name          = "strum-cli"
  spec.version       = Strum::CLI::VERSION
  spec.authors       = ["Serhiy Nazarov"]
  spec.email         = ["sn@nazarov.com.ua"]

  spec.summary       = "Light ruby framework"
  spec.description   = "Strum CLI"
  spec.homepage      = "https://code.qpard.com/strum/strum-cli"
  spec.license       = "MIT"
  spec.required_ruby_version = Gem::Requirement.new(">= 2.7.0")

  spec.metadata["allowed_push_host"] = "https://rubygems.org"

  spec.metadata["homepage_uri"] = spec.homepage
  spec.metadata["source_code_uri"] = "https://code.qpard.com/strum/strum-cli"
  spec.metadata["changelog_uri"] = "https://code.qpard.com/strum/strum-cli/CHANGELOG.md"

  # Specify which files should be added to the gem when it is released.
  # The `git ls-files -z` loads the files in the RubyGem that have been added into git.
  spec.files = Dir.chdir(File.expand_path(__dir__)) do
    `git ls-files -z`.split("\x0").reject { |f| f.match(%r{^(test|spec|features)/}) }
  end
  spec.bindir        = "bin"
  spec.executables   = "strum"
  spec.require_paths = ["lib"]

  spec.add_dependency "dry-inflector", "~> 0.2.0"
  spec.add_dependency "thor", "~> 0.20"
end
