# frozen_string_literal: true

require "thor"

require "strum/cli/mixin/sub_command"
require "strum/cli/generator/model"
require "strum/cli/generator/migration"
require "strum/cli/generator/serializer"
require "strum/cli/generator/route"
require "strum/cli/generator/service"
require "strum/cli/generator/scaffold"

module Strum
  module CLI
    class Generate < Thor
      extend Strum::CLI::Mixin::SubCommand

      register(Strum::CLI::Generator::Service, "service", "service [NAME]", "Create service object")
      register(Strum::CLI::Generator::Route, "route", "route [NAME]", "Create route")
      register(Strum::CLI::Generator::Serializer, "serializer", "serializer [NAME] [fields]", "Create serializer")
      register(Strum::CLI::Generator::Model, "model", "model [NAME] [fields]", "Create sequel model")
      register(Strum::CLI::Generator::Migration, "migration", "migration [NAME] [fields]", "Create sequel migration")
      register(Strum::CLI::Generator::Scaffold, "scaffold", "scaffold [NAME] [fields]", "Create Scaffold")
    end
  end
end
