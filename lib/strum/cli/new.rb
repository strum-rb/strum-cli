# frozen_string_literal: true

require "thor/group"

module Strum
  module CLI
    class New < Thor::Group
      include Thor::Actions

      desc "Create new Strum project skeleton"
      argument :name
      # class_option :cors, type: :boolean
      class_option :stack, type: :array, aliases: ["-s"]

      def create_app
        directory("app/blank", app_name)
        # exec "chmod u+x #{File.join(Dir.pwd, app_name, 'bin/console')}"
        # invoke Strum::CLI::Stack::Sequel, nil, options if sequel?
      end

      def setup_permissions
        Dir.chdir(app_name) do
          Bundler.with_unbundled_env do
            system ("chmod u+x bin/console")
          end
        end
      end

      def bundle_install
        Dir.chdir(app_name) do
          Bundler.with_unbundled_env do
            system ("bundle install")
          end
        end
      end

      def stacks_setup
        thor = Strum::CLI::Thor.new
        cmd = Strum::CLI::Setup.new
        Dir.chdir(app_name) do
          Bundler.with_unbundled_env do
            (options[:stack] || []).each do |stack|
              # thor.invoke("setup", [stack], app_name: app_name)
              cmd.invoke(stack, [], app_name: app_name)
            end
          end
        end
      end

      def self.banner
        "#{basename} #{self_command.formatted_usage(self, false).split(':').last}"
      end

      def self.source_root
        # File.expand_path(".", "templates")
        File.join(File.dirname(__FILE__), "templates")
      end

      protected

        def app_name
          name.gsub(/[^0-9A-Za-z]+/, "_").split(/(?=[A-Z])|[\-_]/).map(&:downcase).join("_")
        end

        def app_class_name
          app_name.split("_").map(&:capitalize).join << "App"
        end
    end
  end
end
