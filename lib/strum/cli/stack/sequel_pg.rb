# frozen_string_literal: true

require "thor/group"

module Strum
  module CLI
    module Stack
      class SequelPG < Thor::Group
        include Thor::Actions
        include Strum::CLI::Mixin::AppCheck
        include Strum::CLI::Mixin::Names

        def pre_check
          raise StrumGenerateError, "This is not Strum application" unless strum?
        end

        def setup_stack
          directory("stack/sequel-pg", ".")
        end

        def add_gemfile
          insert_into_file( "Gemfile", before: "group :development do") do
            "# database\n"
            "gem \"pg\", \"~> 1.2\"\n"
            "gem \"sequel\", \"~> 5.48\"\n"
            "gem \"sequel_pg\", \"~> 1.14\"\n\n"
          end
        end

        def inject_to_roda
          # if roda stack exist
          return unless strum_roda?
          insert_into_file( "config.ru", after: "Rack::Unreloader.new(subclasses: %w[Roda") do
            " Sequel::Model"
          end
        end

        def inject_to_env
          append_to_file("config/env.rb") do
            "Unreloader.require(\"config/sequel\")\n"
          end
        end

        def self.source_root
          File.expand_path("../templates", __dir__)
        end

        protected
          def app_name
            options["app_name"]
          end
      end
    end
  end
end
