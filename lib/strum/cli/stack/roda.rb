# frozen_string_literal: true

require "thor/group"

module Strum
  module CLI
    module Stack
      class Roda < Thor::Group
        include Thor::Actions
        include Strum::CLI::Mixin::AppCheck
        include Strum::CLI::Mixin::Names

        def pre_check
          raise StrumGenerateError, "This is not Strum application" unless strum?
        end

        def setup_stack
          directory("stack/roda", ".")
        end

        def add_gemfile
          insert_into_file( "Gemfile", before: "group :development do") do
            "gem \"roda\", \"~> 3\"\n\n"
          end
        end

        def inject_to_sequel
          # if sequel stack exist
          return unless strum_sequel?
          insert_into_file( "config.ru", after: "Rack::Unreloader.new(subclasses: %w[Roda") do
            " Sequel::Model"
          end
        end

        def self.source_root
          File.expand_path("../templates", __dir__)
        end

        protected
          def app_name
            options["app_name"]
          end

          def app_class_name
            app_name.split("_").map(&:capitalize).join << "App"
          end
      end
    end
  end
end
