# frozen_string_literal: true

require "thor/group"

module Strum
  module CLI
    module Stack
      class Rabbit < Thor::Group
        include Thor::Actions
        include Strum::CLI::Mixin::AppCheck
        include Strum::CLI::Mixin::Names

        def pre_check
          raise StrumGenerateError, "This is not Strum application" unless strum?
        end

        def setup_stack
          directory("app/rabbit")
        end

        def self.source_root
          File.expand_path("../templates", __dir__)
        end
      end
    end
  end
end
