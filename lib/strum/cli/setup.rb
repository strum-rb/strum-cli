# frozen_string_literal: true

require "thor"

require "strum/cli/mixin/sub_command"

require "strum/cli/stack/sequel_pg"
require "strum/cli/stack/roda"
require "strum/cli/stack/rabbit"

module Strum
  module CLI
    class Setup < Thor
      extend Strum::CLI::Mixin::SubCommand

      register(Strum::CLI::Stack::SequelPG, "sequel_pg", "sequel_pg", "Setup Sequel Postgres stack")
      register(Strum::CLI::Stack::Roda, "roda", "roda", "Setup Roda stack")
      register(Strum::CLI::Stack::Rabbit, "rabbit", "rabbit", "Setup Roda stack")
    end
  end
end
