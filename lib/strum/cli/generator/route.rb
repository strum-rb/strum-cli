# frozen_string_literal: true

module Strum
  module CLI
    module Generator
      # Strum generate sub command
      # `strum generate migration`
      class Route < Thor::Group
        include Thor::Actions
        include Strum::CLI::Mixin::AppCheck
        include Strum::CLI::Mixin::Names

        argument :name
        class_option :crud, type: :boolean, aliases: ["-c"]

        def self.source_root
          File.expand_path("../templates", __dir__)
        end

        def pre_check
          raise StrumGenerateError, "This is not Strum application" unless strum?
        end

        def create_route_file
          options[:crud] ? crud_route : basic_route
        end

        protected

          def route_name
            options[:crud] ? inflector.pluralize(name_components.join("-")) : name_components.join("-")
          end

        private

          def basic_route
            directory("route", File.expand_path(namespace_path, "routes"))
          end

          def crud_route
            directory("route_crud", File.expand_path(namespace_path, "routes"))
          end
      end
    end
  end
end
