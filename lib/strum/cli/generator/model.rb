# frozen_string_literal: true

module Strum
  module CLI
    module Generator
      class Model < Thor::Group
        include Thor::Actions
        include Strum::CLI::Mixin::AppCheck
        include Strum::CLI::Mixin::Names

        argument :name, required: true
        argument :fields, type: :array, required: false, default: []
        class_option :rspec, type: :boolean
        class_option :migration, type: :boolean, default: true

        def self.source_root
          File.expand_path("../templates", __dir__)
        end

        def pre_check
          raise StrumGenerateError, "This is not Strum application" unless strum?
          raise StrumGenerateError, "Setup sequel stack before!" unless strum_sequel?
        end

        def create_model_file
          directory("model", File.expand_path(namespace_path, "models"))
        end

        def generate_migration
          Strum::CLI::Generator::Migration.new([name], model: name, fields: options[:fields]).invoke_all if options[:migration]
        end
      end
    end
  end
end
