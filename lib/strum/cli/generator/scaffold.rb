# frozen_string_literal: true

require "strum/cli/generator/model"
require "strum/cli/generator/migration"
require "strum/cli/generator/serializer"
require "strum/cli/generator/route"
require "strum/cli/generator/service"

module Strum
  module CLI
    module Generator
      class Scaffold < Thor::Group
        argument :name, required: true
        argument :fields, type: :array, required: false, default: []
        class_option :crud, type: :boolean, default: true
        class_option :rspec, type: :boolean, default: true

        def create_scaffold
          invoke Strum::CLI::Commands::Model, nil, options
          invoke Strum::CLI::Commands::Migration, nil, model: true
          invoke Strum::CLI::Commands::Serializer, nil, options
          invoke Strum::CLI::Commands::Route, nil, options.merge(crud: true)
          invoke Strum::CLI::Commands::Service, nil, options.merge(crud: true)
        end
      end
    end
  end
end
