# frozen_string_literal: true

require "thor/group"

module Strum
  module CLI
    module Generator
      # Strum generate sub command
      # `strum generate migration`
      class Migration < Thor::Group
        include Thor::Actions
        include Strum::CLI::Mixin::AppCheck
        include Strum::CLI::Mixin::Names

        argument :name
        class_option :model, type: :boolean, default: false
        class_option :fields, type: :array, default: []

        def self.source_root
          File.expand_path("../templates", __dir__)
        end

        def pre_check
          raise StrumGenerateError, "This is not Strum application" unless strum?
          raise StrumGenerateError, "Setup sequel stack before!" unless strum_sequel?
        end

        def create_model_file
          directory("migration", "db/migrations")
        end

        protected

          def model?
            options[:model]
          end

          def table_name
            inflector.tableize(name_components.join("_"))
          end

        private

          def migration_filename
            if model?
              "#{new_migration_number}_create_#{table_name}"
            else
              "#{new_migration_number}_#{name_components.join('_')}"
            end
          end

          def new_migration_number
            migration_files = (Dir.chdir("db/migrations") { Dir.glob("*").max })
            last_number = migration_files && migration_files[/\d+/].to_i || 0
            new_number = last_number > 9 ? last_number + 10 : 10
            new_number.to_s.rjust(3, "0")
          end
      end
    end
  end
end
