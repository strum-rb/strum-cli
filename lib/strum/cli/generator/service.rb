# frozen_string_literal: true

module Strum
  module CLI
    module Generator
      # Strum generate sub command
      # `strum generate service`
      class Service < Thor::Group
        include Thor::Actions
        include Strum::CLI::Mixin::AppCheck
        include Strum::CLI::Mixin::Names
        # include Strum::Commands::Mixin::Fields

        argument :name
        class_option :crud, type: :boolean, aliases: ["-c"]
        class_option :fields, type: :array, default: []

        def self.source_root
          File.expand_path("../templates", __dir__)
        end

        def pre_check
          raise StrumGenerateError, "This is not Strum application" unless strum?
        end

        def create_service_files
          options[:crud] ? crud_service : basic_service
        end

        private

          def basic_service
            directory("service", File.expand_path(namespace_path, "services"))
          end

          def crud_service
            directory("service_crud", File.expand_path(namespace_path, "services"))
          end
      end
    end
  end
end
