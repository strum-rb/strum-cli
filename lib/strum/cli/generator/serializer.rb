# frozen_string_literal: true

module Strum
  module CLI
    module Generator
      # Strum generate sub command
      # `strum generate migration`
      class Serializer < Thor::Group
        include Thor::Actions
        include Strum::CLI::Mixin::AppCheck
        include Strum::CLI::Mixin::Names

        argument :name
        class_option :fields, type: :array, default: []

        def self.source_root
          File.expand_path("../templates", __dir__)
        end

        def pre_check
          raise StrumGenerateError, "This is not Strum application" unless strum?
        end

        def create_serializer_file
          directory("serializer", File.expand_path(namespace_path, "serializers"))
        end
      end
    end
  end
end
