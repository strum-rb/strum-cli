# frozen_string_literal: true

require "thor"
require "strum/cli/mixin/app_check"
require "strum/cli/mixin/names"
require "strum/cli/generate"
require "strum/cli/setup"
require "strum/cli/new"

module Strum
  module CLI
    class Thor < ::Thor
      include Thor::Actions

      map "-v" => :version
          # "g" => :generate,
          # "c" => :console

      register(Strum::CLI::New, "new", "new [NAME] [options]", "Create new project skeleton")

      desc "version", "Show Strum version"
      def version
        require_relative "version"
        say "Strum #{Strum::CLI::VERSION}"
      end

      # desc "console", "Start console"
      # def console
      #   exec File.join(Dir.pwd, "bin/console")
      # end

      desc "generate [RESOURCE]", "Generate new Strum resource"
      subcommand "generate", Strum::CLI::Generate

      desc "setup [STACK]", "Setup Strum stack"
      subcommand "setup", Strum::CLI::Setup
    end
  end
end
