# frozen_string_literal: true

module Strum
  module CLI
    module Mixin
      module SubCommand
        def banner(command, _namespace = nil, _subcommand = false)
          "#{basename} #{subcommand_prefix} #{command.usage}"
        end

        def subcommand_prefix
          name.gsub(/.*::/, "")
              .gsub(/^[A-Z]/) { |match| match[0].downcase }
              .gsub(/[A-Z]/) { |match| "-#{match[0].downcase}" }
        end
      end
    end
  end
end
