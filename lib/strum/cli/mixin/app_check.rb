# frozen_string_literal: true

module Strum
  module CLI
    module Mixin
      # Is it strum folder
      module AppCheck
        protected

          class StrumGenerateError < StandardError; end

          def strum_app?
            File.open("config/env.rb").read.include?('ENV["STRUM_ROOT"]')
          rescue Errno::ENOENT
            false
          end

          def strum?
            strum_app?
          end

          def strum_roda?
            strum? && File.open("app/roda.rb").read.include?('< Roda')
          rescue Errno::ENOENT
            false
          end

          def strum_sequel?
            strum? && File.open("config/sequel.rb").read.include?('DB = Sequel')
          rescue Errno::ENOENT
            false
          end
      end
    end
  end
end
