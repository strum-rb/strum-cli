# frozen_string_literal: true

require "dry-inflector"

module Strum
  module CLI
    module Mixin
      # Parse fields for generator
      module Names
        protected

          def resource_class_name
            resource_name.split("_").map(&:capitalize).join
          end

          def resources_class_name
            inflector.pluralize(resource_class_name)
          end

          def resource_name
            inflector.singularize(name_components.last)
          end

          def resources_name
            inflector.pluralize(name_components.last)
          end
          alias resource_filename resources_name

          def namespace_names
            namespaces.map { |namespace| namespace.split("_").map(&:capitalize).join }
          end

          def namespace_path
            File.join(*namespaces)
          end

          def ident
            "  " * namespaces.size
          end

        private

          def name_components
            @name_components ||= name.scan(/[[:alnum:]]+/).map do |s|
              s.split(/(?=[A-Z])|_/).map(&:downcase).join("_")
            end
          end

          def namespaces
            name_components.take(name_components.size - 1)
          end

          def inflector
            @inflector ||= Dry::Inflector.new
          end
      end
    end
  end
end
