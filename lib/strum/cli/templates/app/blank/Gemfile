# frozen_string_literal: true

source "https://rubygems.org"

gem "bundler", "~> 2.1"
gem "irb", "~> 1.3"

gem "strum-json"
gem "strum-pipe"
gem "strum-service"

group :development do
  # A Ruby code quality reporter
  gem "rubycritic", require: false

  # A Ruby static code analyzer and formatter,
  # based on the community Ruby style guide. https://docs.rubocop.org
  gem "rubocop", "~> 1.20", require: false
  gem "rubocop-rspec", "~> 2.4", require: false
end

group :development, :test do
  # An interface which glues ruby-debug
  gem "debase", "~> 0.2.4"
  gem "ruby-debug-ide", "~> 0.7.2"

  # A Ruby gem to load environment variables from `.env`.
  gem "dotenv"
end
