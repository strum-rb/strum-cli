# frozen_string_literal: true

require "strum/cli/version"
require "strum/cli/thor"

module Strum
  module CLI
    class Error < StandardError; end
  end
end
